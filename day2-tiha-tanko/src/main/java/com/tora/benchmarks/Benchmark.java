package com.tora.benchmarks;

import com.tora.model.Order;
import com.tora.repository.eclipse.FastListBasedRepository;
import com.tora.repository.eclipse.HashBagBasedRepository;
import com.tora.repository.eclipse.UnifiedMapBasedRepository;
import com.tora.repository.eclipse.UnifiedSetBasedRepository;
import com.tora.repository.fastutil.list.ObjectArrayListBasedRepository;
import com.tora.repository.fastutil.list.ReferenceArrayListBasedRepository;
import com.tora.repository.fastutil.map.Int2ObjectArrayMapBasedRepository;
import com.tora.repository.fastutil.map.Int2ObjectOpenHashMapBasedRepository;
import com.tora.repository.fastutil.map.Int2ReferenceArrayMapBasedRepository;
import com.tora.repository.fastutil.map.Int2ReferenceOpenHashMapBasedRepository;
import com.tora.repository.fastutil.set.ObjectArraySetBasedRepository;
import com.tora.repository.fastutil.set.ReferenceArraySetBasedRepository;
import com.tora.repository.jdk.ArrayListBasedRepository;
import com.tora.repository.jdk.ConcurrentHashMapBasedRepository;
import com.tora.repository.jdk.HashSetBasedRepository;
import com.tora.repository.jdk.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Fork(1)
@Warmup(time=2)
@Measurement(time=2)
public class Benchmark {

    @State(Scope.Benchmark)
    public static class ProgramState {

        private static Random random = new Random();
        
        private static List<Order> orders = new ArrayList<Order>(1000){{
           IntStream.range(0, 1000).boxed().forEach(n -> add(new Order(n, 1, 1))); 
        }};

        private ArrayListBasedRepository<Order> arrayListBasedRepository;
        private ConcurrentHashMapBasedRepository<Integer, Order> concurrentHashMapBasedRepository;
        private HashSetBasedRepository<Order> hashSetBasedRepository;
        private TreeSetBasedRepository<Order> treeSetBasedRepository;

        private FastListBasedRepository<Order> fastListBasedRepository;
        private HashBagBasedRepository<Order> hashBagBasedRepository;
        private UnifiedMapBasedRepository<Integer, Order> unifiedMapBasedRepository;
        private UnifiedSetBasedRepository<Order> unifiedSetBasedRepository;

        private ObjectArrayListBasedRepository<Order> objectArrayListBasedRepository;
        private ReferenceArrayListBasedRepository<Order> referenceArrayListBasedRepository;
        private Int2ObjectArrayMapBasedRepository<Order> int2ObjectArrayMapBasedRepository;
        private Int2ObjectOpenHashMapBasedRepository<Order> int2ObjectOpenHashMapBasedRepository;
        private Int2ReferenceArrayMapBasedRepository<Order> int2ReferenceArrayMapBasedRepository;
        private Int2ReferenceOpenHashMapBasedRepository<Order> int2ReferenceOpenHashMapBasedRepository;
        private ObjectArraySetBasedRepository<Order> objectArraySetBasedRepository;
        private ReferenceArraySetBasedRepository<Order> referenceArraySetBasedRepository;

        @Setup(Level.Iteration)
        public void setup() {
            this.arrayListBasedRepository = new ArrayListBasedRepository<>();
            this.concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
            this.hashSetBasedRepository = new HashSetBasedRepository<>();
            this.treeSetBasedRepository = new TreeSetBasedRepository<>();

            this.fastListBasedRepository = new FastListBasedRepository<>();
            this.hashBagBasedRepository = new HashBagBasedRepository<>();
            this.unifiedMapBasedRepository = new UnifiedMapBasedRepository<>();
            this.unifiedSetBasedRepository = new UnifiedSetBasedRepository<>();

            this.objectArrayListBasedRepository = new ObjectArrayListBasedRepository<>();
            this.referenceArrayListBasedRepository = new ReferenceArrayListBasedRepository<>();
            this.int2ObjectArrayMapBasedRepository = new Int2ObjectArrayMapBasedRepository<>();
            this.int2ObjectOpenHashMapBasedRepository = new Int2ObjectOpenHashMapBasedRepository<>();
            this.int2ReferenceArrayMapBasedRepository = new Int2ReferenceArrayMapBasedRepository<>();
            this.int2ReferenceOpenHashMapBasedRepository = new Int2ReferenceOpenHashMapBasedRepository<>();
            this.objectArraySetBasedRepository = new ObjectArraySetBasedRepository<>();
            this.referenceArraySetBasedRepository = new ReferenceArraySetBasedRepository<>();
        }

        @TearDown(Level.Iteration)
        public void tearDown() {
            this.arrayListBasedRepository = null;
            this.concurrentHashMapBasedRepository = null;
            this.hashSetBasedRepository = null;
            this.treeSetBasedRepository = null;

            this.fastListBasedRepository = null;
            this.hashBagBasedRepository = null;
            this.unifiedMapBasedRepository = null;
            this.unifiedSetBasedRepository = null;

            this.objectArrayListBasedRepository = null;
            this.referenceArrayListBasedRepository = null;
            this.int2ObjectArrayMapBasedRepository = null;
            this.int2ObjectOpenHashMapBasedRepository = null;
            this.int2ReferenceArrayMapBasedRepository = null;
            this.int2ReferenceOpenHashMapBasedRepository = null;
            this.objectArraySetBasedRepository = null;
            this.referenceArraySetBasedRepository = null;
        }
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testArrayListBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.arrayListBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testConcurrentHashMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.concurrentHashMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testHashSetBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.hashSetBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testTreeSetBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.treeSetBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testFastListBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.fastListBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testHashBagBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.hashBagBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testUnifiedMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.unifiedMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testUnifiedSetBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.unifiedSetBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testObjectArrayListBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.objectArrayListBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testReferenceArrayListBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.referenceArrayListBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testInt2ObjectArrayMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.int2ObjectArrayMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testInt2ObjectOpenHashMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.int2ObjectOpenHashMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testInt2ReferenceArrayMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.int2ReferenceArrayMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testInt2ReferenceOpenHashMapBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.int2ReferenceOpenHashMapBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testObjectArraySetBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.objectArraySetBasedRepository.add(order);
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void testReferenceArraySetBasedRepository_add(ProgramState state) {
        Order order = ProgramState.orders.get(ProgramState.random.nextInt(ProgramState.orders.size()));
        state.referenceArraySetBasedRepository.add(order);
    }
}
