package com.tora.model;

import java.text.MessageFormat;

public class Order implements Comparable<Order>, Identity<Integer> {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object obj) {
        final Order other = (Order) obj;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }

    @Override
    public String toString() {
        return MessageFormat.format("Order #{0} (price: {1}. quantity: {1})", id, price, quantity);
    }


    @Override
    public int compareTo(Order o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public Integer getId() {
        return this.id;
    }
}
