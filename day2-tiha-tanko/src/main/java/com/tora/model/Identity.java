package com.tora.model;

public interface Identity<K> {
    K getId();
}
