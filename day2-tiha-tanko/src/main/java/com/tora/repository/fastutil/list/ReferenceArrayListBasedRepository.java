package com.tora.repository.fastutil.list;

import com.tora.repository.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ReferenceArrayList;

import java.util.List;

public class ReferenceArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private final List<T> items = new ReferenceArrayList<>();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
