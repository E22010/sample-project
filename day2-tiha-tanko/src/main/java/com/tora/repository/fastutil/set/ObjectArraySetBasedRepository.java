package com.tora.repository.fastutil.set;

import com.tora.repository.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectArraySet;

import java.util.Set;

public class ObjectArraySetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> items = new ObjectArraySet<>();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
