package com.tora.repository.fastutil.set;

import com.tora.repository.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ReferenceArraySet;

import java.util.Set;

public class ReferenceArraySetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> items = new ReferenceArraySet<>();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
