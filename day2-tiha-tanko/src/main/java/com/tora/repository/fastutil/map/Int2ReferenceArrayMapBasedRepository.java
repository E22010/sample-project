package com.tora.repository.fastutil.map;

import com.tora.model.Identity;
import com.tora.repository.InMemoryRepository;
import it.unimi.dsi.fastutil.ints.Int2ReferenceArrayMap;

import java.util.Map;

public class Int2ReferenceArrayMapBasedRepository<T extends Identity<Integer>> implements InMemoryRepository<T> {

    private final Map<Integer, T> items = new Int2ReferenceArrayMap<>();

    @Override
    public void add(T item) {
        items.put(item.getId(), item);
    }

    @Override
    public boolean contains(T item) {
        return items.containsKey(item.getId());
    }

    @Override
    public void remove(T item) {
        items.remove(item.getId());
    }
}
