package com.tora.repository.jdk;

import com.tora.model.Identity;
import com.tora.repository.InMemoryRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<K, T extends Identity<K>> implements InMemoryRepository<T> {

    private final Map<K, T> items = new ConcurrentHashMap<>();

    @Override
    public void add(T item) {
        items.put(item.getId(), item);
    }

    @Override
    public boolean contains(T item) {
        return items.containsKey(item.getId());
    }

    @Override
    public void remove(T item) {
        items.remove(item.getId());
    }
}
