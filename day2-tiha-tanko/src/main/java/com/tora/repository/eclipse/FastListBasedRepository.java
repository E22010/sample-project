package com.tora.repository.eclipse;

import com.tora.repository.InMemoryRepository;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.List;

public class FastListBasedRepository<T> implements InMemoryRepository<T> {

    private final List<T> items = FastList.newList();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
