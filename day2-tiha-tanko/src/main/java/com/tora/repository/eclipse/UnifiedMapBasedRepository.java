package com.tora.repository.eclipse;

import com.tora.model.Identity;
import com.tora.repository.InMemoryRepository;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import java.util.Map;

public class UnifiedMapBasedRepository<K, T extends Identity<K>> implements InMemoryRepository<T> {

    private final Map<K, T> items = UnifiedMap.newMap();

    @Override
    public void add(T item) {
        items.put(item.getId(), item);
    }

    @Override
    public boolean contains(T item) {
        return items.containsKey(item.getId());
    }

    @Override
    public void remove(T item) {
        items.remove(item.getId());
    }
}
