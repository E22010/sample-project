package com.tora.repository.eclipse;

import com.tora.repository.InMemoryRepository;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> items = UnifiedSet.newSet();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
