package com.tora.repository.eclipse;

import com.tora.repository.InMemoryRepository;
import org.eclipse.collections.api.bag.MutableBag;
import org.eclipse.collections.impl.bag.mutable.HashBag;

public class HashBagBasedRepository<T> implements InMemoryRepository<T> {

    private final MutableBag<T> items = HashBag.newBag();

    @Override
    public void add(T item) {
        items.add(item);
    }

    @Override
    public boolean contains(T item) {
        return items.contains(item);
    }

    @Override
    public void remove(T item) {
        items.remove(item);
    }
}
