package serializers;

import com.google.protobuf.ByteString;
import model.BigDecimalOuterClass;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

public class BigDecimalSerializer {
    public static void write(BigDecimal bigDecimal, OutputStream out) throws IOException {
        final BigDecimalOuterClass.BigDecimal wrapper = BigDecimalOuterClass.BigDecimal.newBuilder()
                .setPrecision(bigDecimal.precision())
                .setScale(bigDecimal.scale())
                .setValue(ByteString.copyFrom(bigDecimal.unscaledValue().toByteArray()))
                .build();
        wrapper.writeTo(out);
    }

    public static BigDecimal read(InputStream in) throws IOException {
        final BigDecimalOuterClass.BigDecimal wrapper = BigDecimalOuterClass.BigDecimal.newBuilder()
                .mergeFrom(in)
                .build();
        return new BigDecimal(new BigInteger(wrapper.getValue().toByteArray()), wrapper.getScale());
    }
}
