import serializers.BigDecimalSerializer;
import streams.Streams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        final List<BigDecimal> list = Arrays.asList(
                new BigDecimal(1),
                new BigDecimal(2),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(3),
                new BigDecimal(4)
        );

        final BigDecimal sum = Streams.sum(list);
        final BigDecimal avg = Streams.average(list);
        final List<BigDecimal> top10 = Streams.top10Percent(list);

        try (final FileOutputStream out = new FileOutputStream("day3-tiha-tanko/src/files/sum.txt")) {
            BigDecimalSerializer.write(sum, out);
        }

        try (final FileOutputStream out = new FileOutputStream("day3-tiha-tanko/src/files/avg.txt")) {
            BigDecimalSerializer.write(avg, out);
        }

        try (final FileOutputStream out = new FileOutputStream("day3-tiha-tanko/src/files/top10.txt")) {
            for (BigDecimal bigDecimal : top10) {
                BigDecimalSerializer.write(bigDecimal, out);
            }
        }
    }
}