package streams;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Stream;

public class Streams {
    public static BigDecimal sum(List<BigDecimal> list) {
        return list.stream().reduce(BigDecimal.valueOf(0), BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> list) {
        return sum(list).divide(BigDecimal.valueOf(list.size()), MathContext.DECIMAL32);
    }

    public static List<BigDecimal> top10Percent(List<BigDecimal> list) {
        return list.stream()
                .sorted((bigDecimal1, bigDecimal2) -> -bigDecimal1.compareTo(bigDecimal2))
                .limit(list.size() / 10)
                .toList();
    }
}
