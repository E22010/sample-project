package tests;

import org.junit.jupiter.api.Test;
import streams.Streams;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class TestAverage {
    @Test
    public void emptyList_shouldThrow() {
        final List<BigDecimal> list = new ArrayList<>(0);
        assertThrows(ArithmeticException.class, () -> Streams.average(list));
    }

    @Test
    public void oneItemList_shouldReturnItem() {
        final BigDecimal expected = BigDecimal.TEN;
        final List<BigDecimal> list = List.of(expected);
        final BigDecimal result = Streams.average(list);
        assertEquals(expected, result);
    }

    @Test
    public void twoEvenItemList_shouldReturnAverage() {
        final BigDecimal item1 = BigDecimal.valueOf(2);
        final BigDecimal item2 = BigDecimal.valueOf(10);
        final BigDecimal expected = BigDecimal.valueOf(6);
        final List<BigDecimal> list = List.of(item1, item2);
        final BigDecimal result = Streams.average(list);
        assertEquals(expected, result);
    }

    @Test
    public void twoOddItemList_shouldReturnAverage() {
        final BigDecimal item1 = BigDecimal.valueOf(3);
        final BigDecimal item2 = BigDecimal.valueOf(9);
        final BigDecimal expected = BigDecimal.valueOf(6);
        final List<BigDecimal> list = List.of(item1, item2);
        final BigDecimal result = Streams.average(list);
        assertEquals(expected, result);
    }

    @Test
    public void oddAndEvenItemList_shouldReturnAverage() {
        final BigDecimal item1 = BigDecimal.valueOf(2);
        final BigDecimal item2 = BigDecimal.valueOf(11);
        final BigDecimal expected = BigDecimal.valueOf(6.5d);
        final List<BigDecimal> list = List.of(item1, item2);
        final BigDecimal result = Streams.average(list);
        assertEquals(expected, result);
    }

    @Test
    public void manyItemsList_shouldReturnSum() {
        final int until = 10000;
        final List<BigDecimal> list = IntStream.range(0, until).boxed()
                .map(BigDecimal::valueOf)
                .toList();
        final BigDecimal expected = BigDecimal.valueOf(9999).divide(BigDecimal.TWO);
        final BigDecimal result = Streams.average(list);
        assertEquals(expected, result);
    }
}
