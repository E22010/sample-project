package tests;

import org.junit.jupiter.api.Test;
import streams.Streams;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class TestSum {
    @Test
    public void emptyList_shouldReturnZero() {
        final List<BigDecimal> list = new ArrayList<>(0);
        final BigDecimal result = Streams.sum(list);
        final BigDecimal expected = BigDecimal.ZERO;
        assertEquals(expected, result);
    }

    @Test
    public void oneItemList_shouldReturnItem() {
        final BigDecimal expected = BigDecimal.TEN;
        final List<BigDecimal> list = List.of(expected);
        final BigDecimal result = Streams.sum(list);
        assertEquals(expected, result);
    }

    @Test
    public void twoItemList_shouldReturnSum() {
        final BigDecimal item1 = BigDecimal.valueOf(2);
        final BigDecimal item2 = BigDecimal.valueOf(10);
        final BigDecimal expected = BigDecimal.valueOf(12);
        final List<BigDecimal> list = List.of(item1, item2);
        final BigDecimal result = Streams.sum(list);
        assertEquals(expected, result);
    }

    @Test
    public void manyItemsList_shouldReturnSum() {
        final int until = 10000;
        final List<BigDecimal> list = IntStream.range(0, until).boxed()
                .map(BigDecimal::valueOf)
                .toList();
        final BigDecimal expected = BigDecimal.valueOf(5000).multiply(BigDecimal.valueOf(9999));
        final BigDecimal result = Streams.sum(list);
        assertEquals(expected, result);
    }
}