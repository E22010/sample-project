package tests;

import org.junit.jupiter.api.Test;
import serializers.BigDecimalSerializer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;


public class TestSerializer {

    @Test
    public void zeroShouldMatchItself() throws IOException {
        assertPreservedWhenSerialized(BigDecimal.ZERO);
    }

    @Test
    public void oneShouldMatchItself() throws IOException {
        assertPreservedWhenSerialized(BigDecimal.ONE);
    }

    @Test
    public void oneMillionShouldMatchItself() throws IOException {
        assertPreservedWhenSerialized(BigDecimal.valueOf(1000000));
    }

    @Test
    public void complicatedDecimalShouldMatchItself() throws IOException {
        assertPreservedWhenSerialized(BigDecimal.valueOf(-92582.985230001));
    }

    private static void assertPreservedWhenSerialized(BigDecimal number) throws IOException {
        try (final FileOutputStream out = new FileOutputStream("files/sum.txt")) {
            BigDecimalSerializer.write(number, out);
        }

        try (final FileInputStream in = new FileInputStream("files/sum.txt")) {
            final BigDecimal result = BigDecimalSerializer.read(in);
            assertEquals(number, result);
        }
    }
}
