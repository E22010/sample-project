package org.example.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

class Connection implements Runnable {

    private final String hostName;
    private final int portNumber;

    public Connection(String hostName, int portNumber) {
        this.hostName = hostName;
        this.portNumber = portNumber;
    }

    @Override
    public void run() {

        System.out.println("Trying to connect to " + portNumber);
        try (Socket socket = new Socket(hostName, portNumber)) {
            System.out.printf("Connected to %s:%d%n", hostName, portNumber);
            final PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.printf("!hello %s:%d%n", hostName, portNumber);

            final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            final String received = in.readLine();
            System.out.println("Received: " + received);

            System.out.println("Input message");
            final Scanner scanner = new Scanner(System.in);
            final String toSend = scanner.nextLine();
            out.println(toSend);

            final String goodbye = in.readLine();
            System.out.println("Receiver: " + goodbye);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
