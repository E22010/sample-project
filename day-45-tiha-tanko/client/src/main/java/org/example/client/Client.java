package org.example.client;

import org.example.protocol.Protocol;
import org.example.protocol.ProtocolException;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Client implements Runnable {
    private boolean running;

    private final Pattern inputPattern = Pattern.compile("\s*(!hello)\s+(.*):(\\d*)");
    private final Protocol<Matcher> inputProtocol = input -> {
        final Matcher matcher = inputPattern.matcher(input);
        if (!matcher.matches()) {
            throw new ProtocolException();
        } else {
            return matcher;
        }
    };

    @Override
    public void run() {
        running = true;

        final Scanner scanner = new Scanner(System.in);
        while (running) {
            final String input = scanner.nextLine();
            try {
                final Matcher matcher = inputProtocol.tryParse(input);
                final String hostName = matcher.group(2);
                final int portNumber = Integer.parseInt(matcher.group(3));
                connect(hostName, portNumber);
            } catch (ProtocolException e) {
                printHelp();
            }
        }
    }

    private void connect(String hostName, int portNumber) {
        final Connection connection = new Connection(hostName, portNumber);
        connection.run();
    }

    private void printHelp() {
        System.out.println("You can initiate a connection using !hello <name>");
    }
}
