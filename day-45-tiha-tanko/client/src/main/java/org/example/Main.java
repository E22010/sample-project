package org.example;

import org.example.client.Client;
import org.example.server.Server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) {
        final String hostName = args[0];
        final int portNumber = Integer.parseInt(args[1]);
        final List<Future<?>> futures = new ArrayList<>(2);
        try {
            final ExecutorService executorService = Executors.newCachedThreadPool();
            final Server server = new Server(portNumber);
            final Client client = new Client();
            futures.add(executorService.submit(server));
            futures.add(executorService.submit(client));
            for (Future<?> future : futures) {
                future.get();
            }
            executorService.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}