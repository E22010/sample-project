package org.example.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

class Connection implements Runnable {
    private final Socket socket;

    Connection(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Connected");
        try {
            final PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out.println("Greetings");
            System.out.println("Waiting for message");
            final String message = in.readLine();

            System.out.println("Received: " + message);
            out.println("Goodbye");

            System.out.println("Disconnecting");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
