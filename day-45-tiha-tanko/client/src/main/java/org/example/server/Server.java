package org.example.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private final int portNumber;

    public Server(int portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public void run() {
        try (final ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("Waiting for connection");
            final Socket clientSocket = serverSocket.accept();
            final Connection connection = new Connection(clientSocket);
            connection.run();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
