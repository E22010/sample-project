package org.example.protocol;

@FunctionalInterface
public interface Protocol<R> {
    R tryParse(String string) throws ProtocolException;
}
