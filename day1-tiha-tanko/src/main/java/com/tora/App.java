package com.tora;

import com.tora.parsers.*;
import com.tora.resolvers.BinaryOperatorResolver;
import com.tora.resolvers.ReferenceResolver;
import com.tora.resolvers.UnaryOperatorResolver;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        final BinaryOperatorResolver additionResolver = new BinaryOperatorResolver(Double::sum);
        final BinaryOperatorParser additionParser = new BinaryOperatorParser(
                NumberParser.PATTERN + "\\+" + NumberParser.PATTERN,
                additionResolver
        );

        final BinaryOperatorResolver subtractionResolver = new BinaryOperatorResolver((term1, term2) -> term1 - term2);
        final BinaryOperatorParser subtractionParser = new BinaryOperatorParser(
                NumberParser.PATTERN + "-" + NumberParser.PATTERN,
                subtractionResolver
        );

        final BinaryOperatorResolver multiplicationResolver = new BinaryOperatorResolver((term1, term2) -> term1 * term2);
        final BinaryOperatorParser multiplicationParser = new BinaryOperatorParser(
                NumberParser.PATTERN + "\\*" + NumberParser.PATTERN,
                multiplicationResolver
        );

        final BinaryOperatorResolver divisionResolver = new BinaryOperatorResolver((term1, term2) -> term1 / term2);
        final BinaryOperatorParser divisionParser = new BinaryOperatorParser(
                NumberParser.PATTERN + "/" + NumberParser.PATTERN,
                divisionResolver
        );

        final BinaryOperatorResolver minimumResolver = new BinaryOperatorResolver(Math::min);
        final BinaryOperatorParser minimumParser= new BinaryOperatorParser(
                "min\\(" + NumberParser.PATTERN + "," + NumberParser.PATTERN + "\\)",
                minimumResolver
        );

        final BinaryOperatorResolver maximumResolver = new BinaryOperatorResolver(Math::max);
        final BinaryOperatorParser maximumParser = new BinaryOperatorParser(
                "max\\(" + NumberParser.PATTERN + "," + NumberParser.PATTERN + "\\)",
                maximumResolver
        );

        final UnaryOperatorResolver squareRootResolver = new UnaryOperatorResolver(Math::sqrt);
        final UnaryOperatorParser squareRootParser = new UnaryOperatorParser(
                "sqrt\\(" + NumberParser.PATTERN + "\\)",
                squareRootResolver
        );

        final ReferenceResolver referenceResolver = new ReferenceResolver();
        final ReferenceParser referenceParser = new ReferenceParser(
                referenceResolver
        );

        final LineParser parser = new LineParser(Arrays.asList(
                additionParser, subtractionParser,
                multiplicationParser, divisionParser,
                minimumParser, maximumParser,
                squareRootParser, referenceParser
        ));

        readUntilExit(parser, referenceResolver);
    }

    private static void readUntilExit(LineParser parser, ReferenceResolver referenceResolver) {
        final Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        while (!input.equals("exit") && parser.canParse(input)) {
            double result = parser.parse(input).resolve();
            referenceResolver.record(result);
            System.out.println(result);
            input = scanner.nextLine();
        }
    }
}
