package com.tora.parsers;

import com.tora.expressions.UnaryOperator;
import com.tora.resolvers.Resolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UnaryOperatorParser implements Parser<UnaryOperator> {

    private final Pattern pattern;

    private final Resolver<UnaryOperator> resolver;

    public UnaryOperatorParser(String pattern, Resolver<UnaryOperator> resolver) {
        this.pattern = Pattern.compile(pattern);
        this.resolver = resolver;
    }

    @Override
    public boolean canParse(String string) {
        return pattern.matcher(string).find();
    }

    @Override
    public UnaryOperator parse(String string) {
        final Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            throw new ParserException(string);
        }
        final String group = matcher.group(1);
        final double value = Double.parseDouble(group);
        return new UnaryOperator(value, resolver);
    }
}
