package com.tora.parsers;

import com.tora.expressions.Number;
import com.tora.resolvers.Resolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberParser implements Parser<Number> {

    public final static String PATTERN = "([+-]?\\d+\\.?\\d*)";
    private final static Pattern pattern = Pattern.compile(PATTERN);

    private final Resolver<Number> resolver;

    public NumberParser(Resolver<Number> resolver) {
        this.resolver = resolver;
    }

    @Override
    public boolean canParse(String string) {
        return pattern.matcher(string).find();
    }

    @Override
    public Number parse(String string) {
        final Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            throw new ParserException(string);
        }

        final String group = matcher.group(1);
        final double value = Double.parseDouble(group);
        return new Number(value, resolver);
    }
}
