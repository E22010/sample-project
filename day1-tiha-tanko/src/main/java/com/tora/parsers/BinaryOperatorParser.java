package com.tora.parsers;

import com.tora.expressions.BinaryOperator;
import com.tora.resolvers.Resolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BinaryOperatorParser implements Parser<BinaryOperator> {

    private final Pattern pattern;

    private final Resolver<BinaryOperator> resolver;

    public BinaryOperatorParser(String pattern, Resolver<BinaryOperator> resolver) {
        this.pattern = Pattern.compile(pattern);
        this.resolver = resolver;
    }

    @Override
    public boolean canParse(String string) {
        return pattern.matcher(string).find();
    }

    @Override
    public BinaryOperator parse(String string) {
        final Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            throw new ParserException(string);
        }

        final String group1 = matcher.group(1);
        final String group2 = matcher.group(2);
        final double value1 = Double.parseDouble(group1);
        final double value2 = Double.parseDouble(group2);
        return new BinaryOperator(value1, value2, resolver);
    }
}
