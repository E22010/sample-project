package com.tora.parsers;

import com.tora.expressions.Expression;

import java.util.List;

public class LineParser implements Parser<Expression<?>> {

    private final List<Parser<? extends Expression<?>>> parsers;

    public LineParser(List<Parser<? extends Expression<?>>> parsers) {
        this.parsers = parsers;
    }

    @Override
    public boolean canParse(String string) {
        return parsers.stream().anyMatch(parser -> parser.canParse(string));
    }

    @Override
    public Expression<?> parse(String string) {
        return parsers.stream()
                .filter(parser -> parser.canParse(string))
                .findFirst()
                .map(parser -> parser.parse(string))
                .orElseThrow(() -> new ParserException(string));
    }
}
