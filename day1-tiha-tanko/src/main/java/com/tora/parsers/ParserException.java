package com.tora.parsers;

public class ParserException extends RuntimeException {
    public ParserException(String message) {
        super("Could not parse " + message);
    }
}
