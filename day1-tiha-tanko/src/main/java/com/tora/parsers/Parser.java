package com.tora.parsers;

public interface Parser<T> {
    boolean canParse(String string);

    T parse(String string);
}
