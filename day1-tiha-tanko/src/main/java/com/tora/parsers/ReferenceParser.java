package com.tora.parsers;

import com.tora.expressions.Reference;
import com.tora.resolvers.Resolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReferenceParser implements Parser<Reference> {

    private final static String UINT_PATTERN = "(\\d+)";
    private final static String PATTERN = "ans(~" + UINT_PATTERN + ")?";

    private final Pattern pattern = Pattern.compile(PATTERN);

    private final Resolver<Reference> resolver;

    public ReferenceParser(Resolver<Reference> resolver) {
        this.resolver = resolver;
    }

    @Override
    public boolean canParse(String string) {
        return pattern.matcher(string).find();
    }

    @Override
    public Reference parse(String string) {
        final Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            throw new ParserException(string);
        }
        final String match = matcher.group(2);
        return match == null ? new Reference(resolver) : new Reference(Integer.parseInt(match), resolver);
    }
}
