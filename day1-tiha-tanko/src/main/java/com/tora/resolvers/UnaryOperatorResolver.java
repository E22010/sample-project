package com.tora.resolvers;

import com.tora.expressions.Expression;
import com.tora.expressions.UnaryOperator;

public class UnaryOperatorResolver implements Resolver<UnaryOperator> {

    @FunctionalInterface
    public interface UnaryOperatorFunction {
        double apply(double term);
    }

    private final UnaryOperatorFunction function;

    public UnaryOperatorResolver(UnaryOperatorFunction function) {
        this.function = function;
    }

    @Override
    public double resolve(UnaryOperator expression) {
        return function.apply(expression.getTerm());
    }
}
