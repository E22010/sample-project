package com.tora.resolvers;

import com.tora.expressions.BinaryOperator;
import com.tora.expressions.Expression;

public class BinaryOperatorResolver implements Resolver<BinaryOperator> {

    @FunctionalInterface
    public interface BinaryOperatorFunction {
        double apply(double term1, double term2);
    }

    private final BinaryOperatorFunction function;

    public BinaryOperatorResolver(BinaryOperatorFunction function) {
        this.function = function;
    }

    @Override
    public double resolve(BinaryOperator expression) {
        return function.apply(expression.getTerm1(), expression.getTerm2());
    }
}
