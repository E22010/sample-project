package com.tora.resolvers;

import com.tora.expressions.Expression;
import com.tora.expressions.Number;

public class NumberResolver implements Resolver<Number> {
    @Override
    public double resolve(Number expression) {
        return expression.getValue();
    }
}
