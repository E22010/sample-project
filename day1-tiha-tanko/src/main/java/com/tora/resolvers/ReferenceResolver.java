package com.tora.resolvers;

import com.tora.expressions.Expression;
import com.tora.expressions.Reference;

import java.util.ArrayList;
import java.util.List;

public class ReferenceResolver implements Resolver<Reference> {

    private final List<Double> results = new ArrayList<>();

    @Override
    public double resolve(Reference expression) {
        final int reference = expression.getReference();
        return results.get(results.size() - reference - 1);
    }

    public void record(double result) {
        results.add(result);
    }
}
