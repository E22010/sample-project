package com.tora.resolvers;

import com.tora.expressions.Expression;

public interface Resolver<T extends Expression<T>> {
    double resolve(T expression);
}
