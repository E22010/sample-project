package com.tora.expressions;

import com.tora.resolvers.Resolver;

public class UnaryOperator implements Expression<UnaryOperator> {
    private final double term;

    private final Resolver<UnaryOperator> resolver;

    public UnaryOperator(double term, Resolver<UnaryOperator> resolver) {
        this.term = term;
        this.resolver = resolver;
    }

    public double getTerm() {
        return term;
    }

    @Override
    public double resolve() {
        return resolver.resolve(this);
    }
}
