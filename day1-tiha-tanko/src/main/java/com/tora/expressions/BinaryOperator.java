package com.tora.expressions;

import com.tora.resolvers.Resolver;

public class BinaryOperator implements Expression<BinaryOperator> {
    private final double term1;
    private final double term2;

    private final Resolver<BinaryOperator> resolver;

    public BinaryOperator(double term1, double term2, Resolver<BinaryOperator> resolver) {
        this.term1 = term1;
        this.term2 = term2;
        this.resolver = resolver;
    }

    public double getTerm1() {
        return term1;
    }

    public double getTerm2() {
        return term2;
    }

    @Override
    public double resolve() {
        return resolver.resolve(this);
    }
}
