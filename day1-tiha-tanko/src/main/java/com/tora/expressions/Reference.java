package com.tora.expressions;

import com.tora.resolvers.Resolver;

public class Reference implements Expression<Reference> {
    private final int reference;

    private final Resolver<Reference> resolver;

    public Reference(Resolver<Reference> resolver) {
        this.resolver = resolver;
        this.reference = 0;
    }

    public Reference(int reference, Resolver<Reference> resolver) {
        this.reference = reference;
        this.resolver = resolver;
    }

    public int getReference() {
        return this.reference;
    }

    @Override
    public double resolve() {
        return resolver.resolve(this);
    }
}
