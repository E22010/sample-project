package com.tora.expressions;

import com.tora.resolvers.Resolver;

public class Number implements Expression<Number> {
    private final double value;

    private final Resolver<Number> resolver;

    public Number(double value, Resolver<Number> resolver) {
        this.value = value;
        this.resolver = resolver;
    }

    public double getValue() {
        return value;
    }

    @Override
    public double resolve() {
        return resolver.resolve(this);
    }
}
