package com.tora.expressions;

public interface Expression<T extends Expression<T>> {
    double resolve();
}
